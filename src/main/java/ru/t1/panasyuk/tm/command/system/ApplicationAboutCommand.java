package ru.t1.panasyuk.tm.command.system;

public final class ApplicationAboutCommand extends AbstractSystemCommand {

    private static final String ARGUMENT = "-a";

    private static final String DESCRIPTION = "Show developer info.";

    private static final String NAME = "about";

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("Name: Petr Panasyuk");
        System.out.println("E-mail: ppanasyuk@t1-consulting.ru");
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

}