package ru.t1.panasyuk.tm.command.system;

import ru.t1.panasyuk.tm.api.service.ICommandService;
import ru.t1.panasyuk.tm.command.AbstractCommand;
import ru.t1.panasyuk.tm.enumerated.Role;

public abstract class AbstractSystemCommand extends AbstractCommand {

    protected ICommandService getCommandService() {
        return getServiceLocator().getCommandService();
    }

    @Override
    public Role[] getRoles() {
        return null;
    }

}