package ru.t1.panasyuk.tm.command.user;

import ru.t1.panasyuk.tm.enumerated.Role;

public final class UserLogoutCommand extends AbstractUserCommand {

    private final String NAME = "user-logout";

    private final String DESCRIPTION = "Logout current user.";

    @Override
    public void execute() {
        System.out.println("[USER LOGOUT]");
        getAuthService().logout();
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}