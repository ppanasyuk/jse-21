package ru.t1.panasyuk.tm.service;

import ru.t1.panasyuk.tm.api.repository.IUserOwnedRepository;
import ru.t1.panasyuk.tm.api.service.IUserOwnedService;
import ru.t1.panasyuk.tm.enumerated.Sort;
import ru.t1.panasyuk.tm.exception.entity.EntityNotFoundException;
import ru.t1.panasyuk.tm.exception.field.IndexIncorrectException;
import ru.t1.panasyuk.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public abstract class AbstractUserOwnedService<M extends AbstractUserOwnedModel, R extends IUserOwnedRepository<M>>
        extends AbstractService<M, R> implements IUserOwnedService<M> {

    public AbstractUserOwnedService(final R repository) {
        super(repository);
    }

    @Override
    public M add(final String userId, final M model) {
        if (userId == null) return null;
        model.setUserId(userId);
        return add(model);
    }

    @Override
    public void clear(final String userId) {
        repository.clear(userId);
    }

    @Override
    public boolean existsById(final String userId, final String id) {
        return repository.existsById(userId, id);
    }

    @Override
    public List<M> findAll(final String userId) {
        return repository.findAll(userId);
    }

    @Override
    public List<M> findAll(final String userId, final Comparator<M> comparator) {
        return repository.findAll(userId, comparator);
    }

    @Override
    public M findOneById(final String userId, final String id) {
        if (userId == null || id == null) return null;
        return repository.findOneById(userId, id);
    }

    @Override
    public M findOneByIndex(final String userId, final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= getSize(userId)) throw new IndexIncorrectException();
        return repository.findOneByIndex(userId, index);
    }

    @Override
    public int getSize(final String userId) {
        return repository.getSize(userId);
    }

    @Override
    public M remove(final String userId, final M model) {
        if (userId == null || model == null) throw new EntityNotFoundException();
        return removeById(userId, model.getId());
    }

    @Override
    public M removeById(final String userId, final String id) {
        if (userId == null || id == null) return null;
        final M model = repository.removeById(userId, id);
        if (model == null) throw new EntityNotFoundException();
        return model;
    }

    @Override
    public M removeByIndex(final String userId, final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= getSize(userId)) throw new IndexIncorrectException();
        final M model = repository.removeByIndex(userId, index);
        if (model == null) throw new EntityNotFoundException();
        return model;
    }

    @Override
    public List<M> findAll(final String userId, final Sort sort) {
        if (userId == null) return null;
        if (sort == null) return findAll(userId);
        final Comparator<M> comparator = sort.getComparator();
        if (comparator == null) return findAll(userId);
        return findAll(userId, comparator);
    }

}