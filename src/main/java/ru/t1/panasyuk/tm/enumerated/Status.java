package ru.t1.panasyuk.tm.enumerated;

public enum Status {

    NOT_STARTED("Not started"),
    IN_PROGRESS("In progress"),
    COMPLETED("Completed");

    private static String displayNames = "";

    public static String getDisplayNames() {
        if (!Status.displayNames.isEmpty()) return Status.displayNames;
        final StringBuilder displayNames = new StringBuilder();
        int index = 0;
        for (final Status status : values()) {
            displayNames.append(status.getDisplayName());
            if (index < values().length - 1) displayNames.append(", ");
            index++;
        }
        Status.displayNames = "[" + displayNames + "]";
        return Status.displayNames;
    }

    public static String toName(final Status status) {
        if (status == null) return null;
        return status.getDisplayName();
    }

    public static Status toStatus(final String value) {
        if (value == null || value.isEmpty()) return null;
        for (final Status status : values()) {
            if (status.getDisplayName().equals(value))
                return status;
        }
        return null;
    }

    private final String displayName;

    Status(final String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }

}