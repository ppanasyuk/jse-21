package ru.t1.panasyuk.tm.api.service;

import ru.t1.panasyuk.tm.enumerated.Role;
import ru.t1.panasyuk.tm.model.User;

public interface IAuthService {

    void checkRoles(Role[] roles);

    User registry(String login, String password, String email);

    void login(String login, String password);

    void logout();

    boolean isAuth();

    String getUserId();

    User getUser();

}